package nl.bioinf.wis1.users;

public class Address {
    private int number;
    private String numberPostfix;
    private String zipCode;
    private String street;
    private String city;

    public Address(int number, String numberPostfix, String zipCode, String street, String city) {
        this.number = number;
        this.numberPostfix = numberPostfix;
        this.zipCode = zipCode;
        this.street = street;
        this.city = city;
    }

    public Address(int number, String zipCode, String street, String city) {
        this(number, null, zipCode, street, city);
    }

    public int getNumber() {
        return number;
    }

    public String getNumberPostfix() {
        return numberPostfix;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getPrintableAddress() {
        return getStreet() + " " + getNumber() + " " + getZipCode() + " " + getCity();
    }

    @Override
    public String toString() {
        return "Address{" +
                "number=" + number +
                ", numberPostfix='" + numberPostfix + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", street='" + street + '\'' +
                ", city='" + city + '\'' +
                '}';
    }


}
